# Workflow and Implementation of `gmxbm.py`
## `run` module

The usage of `run` module is done in two steps as follows:

1. Setting up machine configuration.
2. Running `gmxbm.py run` with command line arguments.

The workflow of `gmxbm.py run` module can be describe as follows:

-  `gmxbm.py run` module
  1. Parses user's input from the command line interface to generate benchmark test case dynamically.
  2. Spawns child process to initiate [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) and exit
      1. Child process initiate [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) and wait for [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) to finish
        1. [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) initiated benchmarking pipeline and waits for benchmarking to be finished.
      2. Child process generate a combined static html file for all the tests available in the output directory speficied by the user and exit.



#### Pre processing
###### Setting up machine configuration
`Cluster`, `partition`, `programming environments` for each benchmark test all are set by users in the machine Configuration file. This can be hardcoded in the file `configurations/config.py`. Or, users can provide their own machine configuration file as well using `-C/--config-file` command line argument of `gmxbm.py`. The configuration file has to be hardcoded using the specifications defined by [ReFrame Configuring Site](https://reframe-hpc.readthedocs.io/en/stable/configure.html).

__A quick guide on how to set up machine in `configurations/config.py`__ :

`configurations/config.py` contains a single varibale named `site_configuration`, which is a `Python` `dict` datatype.
`systems` key within this dictionary holds a list of machine settings. Each machine within this list is again a `Python` `dict` datatype. A code snippet of such `Python` `dict` representing a `Beskow` at `PDC` with two partition:
```
'systems': [
    {
        'name': 'Beskow',
        'descr': 'Beskow at PDC',
        'hostnames': ['\s*\.*?beskow.*'],
        'modules_system': 'tmod4',
        'partitions': [
            {
                'name': 'interactive_module',
                'scheduler': 'local',
                'launcher': 'srun',
                'environs': ['gromacs_2020_4'],
            },
            {
                'name': 'queue',
                'scheduler': 'slurm',
                'launcher': 'srun',
                'access': ['-A <project_name>'],
                'environs': ['gromacs_2020_1','gromacs_2020_4'],
            },
        ],
    },
]
```
Entries of the list specified in the `environs` key in the above code snippet, represents the `programming environment` that should be used for a test and a user can specify that when running `run` module of `gmxbm.py`. More on it are in the following section. But, a `programming environment` has to be defined before it can be used. A `programming environment` is defined as a entry of type `Python` `dict` within the `list` represented by `environments` keys in `site_configuration`. Code snippet on on specifying `gromacs_2020_1` and `gromacs_2020_4` is follows:

```
'environments': [
    {
        'name': 'gromacs_2020_1',
        'modules': ['gromacs/2020.1']
    },
    {
        'name': 'gromacs_2020_4',
        'modules': ['gromacs/2020.4']
    },
],
```
`modules` in the above code snippet represents a `list` of modules that needs to be to activate the `programming environment` specified by `name`.
Detailed information on machine settings can be found on [ReFrame Configuring Site](https://reframe-hpc.readthedocs.io/en/stable/configure.html).

---

#### `gmxbm.py run` module
The job of this module is to generate test cases based on the user's input and initiate [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) to start benchmarking through a child process. The reason of using child process is to run [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) on the background. The purpose and possibility of extension of some of the scripts within this module are follows:

`apps/gromacs/test.py`:

This script defines the `Python` class `GromacsRegressionTest`, that is used by [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) to generate the instance of benchmark test case dynamically based on the users input passed through the command line argument of `gmxbm.py run` module. To add more information to the report file one needs to modify the `extract_info` method of this class along with the scripts `gmxbmcore/parser.py` and `gmxbmcore/reports.py`.

`configurations/config.py`:

This scripts specify the machine configurations. Details of this scripts has provided in the previous section (__Setting up machine configuration__)

`configurations/perf_config.py`:

This script is used to specify performance variable that will be added to the `JSON` formatted report file under the `performance` section. Optionally, it is possible to add reference value for each of the performance variable. This script contains two variable of type `Python` dictionary to specify the performance variables and the associated reference value, which are presented in the following code snippets:
```
perf_patterns = {
    'ns/day': gmx_sanity_functions.ns_per_day,
}

reference = {
    'Tegner': {
        'ns/day': (None, None, None, None),
    },
    'Beskow': {
        'ns/day': (None, None, None, None),
    },
}
```
Performance variables are specified as key/value pair within `perf_patterns`, where key is the names of the variable and value is a sanity function specified in  `gmxbmcore/sanity_functions.py` . Value specified in `perf_patterns` is used in `__init__` method of `GromacsRegressionTest` in script `apps/gromacs/test.py`.


Reference value for performance variables for each machines specified in the machine config file are specified within `reference` dictionary. In this case, key is the name of the machine, and the value is again `Python` dictionary. `Python` dictionary associated with each machine, takes performance varialbe specified in `perf_patterns` as key and a performance reference tuple as the corresponding value. The performance reference tuple consists of the reference value, the lower and upper thresholds expressed as fractional numbers relative to the reference value, and the unit of measurement. If any of the thresholds is not relevant, `None` may be used instead. Value specified in `reference` are used in `add_performance_reference` method of `GromacsRegressionTest` in script `apps/gromacs/test.py`.


`gmxbmcore/sanity_functions.py`:

This script contains two sanity functions to collect performance data for `ns/day` and `hr/ns` from the `GROMACS` log output. These functions are wappers around the original [ReFrame sanity functions](https://reframe-hpc.readthedocs.io/en/stable/sanity_functions_reference.html) call and takes the test instance as the only argument. Function used as the value in `perf_patterns` within script `configurations/perf_config.py` need to be wrapper of the following type:

```
import reframe.utility.sanity as sn
@sn.sanity_function
def ns_per_day(obj):
    nspd = sn.extractsingle(
        r'^Performance:\s+(?P<nspd>\S+)\s+.*',
        obj.output_file,
        'nspd',
        float
    )

    return nspd
```

Importantly, all the wrappers to the [ReFrame sanity functions](https://reframe-hpc.readthedocs.io/en/stable/sanity_functions_reference.html) in this script needs to be decorated by `reframe.utility.sanity.sanity_function` to defer the execution for later time.


`configurations/sanity_config.py`:

This scripts contains two different kinds of sanity checking. `assert_found` tuple within this script can be extended to check the existance of a specified string within the `GROMACS` log output. Current implementation only check the existance of `Finished mdrun`. On the other hand,  `assert_reference` dictionary can be used to check whether the value of a variable from `GROMACS` log output is within the threshold of a reference value. One thing to remember when extending `assert_reference`, key to `assert_reference` and the tag within `pattern` in the dictionary associated with the key should be same. Keys to dictionary associated with each key in `assert_reference` are: `pattern, ref_value, lower_thres and upper_thres`. `pattern` expects a regular expression and if any of the thresholds is not relevant, `None` may be used instead. Both `assert_found` and `assert_reference` are consumed in `add_sanity` method of `GromacsRegressionTest` in script `apps/gromacs/test.py`. Code snippet of this script:
```
assert_found = (
    r'Finished mdrun',
)

assert_reference = dict(
    walltime={
        'pattern': (r'\s*Time:\s*\d*\.\d*\s*(?P<walltime>\d*\.\d*)\s*\d*\.\d*'),
        'ref_value': 30.0,
        'lower_thres': 0.0,
        'upper_thres': None
    }
)
```

`gmxbmcore/parser.py`:

This script contains some utility functions to extract data from `GROMACS` log file, which are not considered to be performance variables, such as `GROMACS` binary info, cpu info, gpu info, compilers info etc. This script can be extended to parse more info from `GROMACS` log file and can be used in `extract_info` method of `GromacsRegressionTest` in script `apps/gromacs/test.py`.

`gmxbmcore/reports.py`:

`GMXPerformanceReport` class in this script is responsible to generate the `JSON` formatted report file along with static html file for each of the test.

## Running `gmxbm.py run`
Information on how to run this module can be found with example commands in `HOW_TO_USE.md` file.

---

## Outputs
By default, all the outputs from the tests are stored in the working directory from where the top level script `gmxbm.py` is run. However, users can specify specific root of the output directory by using the `--outputdir` option while running `gmxbm.py run`. If the root of the output directory already contains results from previous tests, new test results will be automatically aggregated alongside that.

List of generated folders within output root:

1. `perflogs`
2. `reports`
3. `gmx-outputs`
4. `statichtml`
5. `logs`

#### `perflogs`
This directory is generated by [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) and the `GromacsRegressionTest.log` file within the directory contains single liner record for each tests ran using the same `cluster` and `partition` combination. Tree view of the `perflogs` directory:

```
perflogs
└─── cluster_name
│   └─── partition_name
│       └─── GromacsRegressionTest.log
└─── cluster_name
|    └─── partition_name
|        └─── GromacsRegressionTest.log
```


#### `reports`
This directory is generated by `gmxbm.py` and contains in-depth report for each test. This directory structure is as `perflogs` folder except that nesting directories start with `GROMACS` version, followed by `cluster` and `partition` name and for each test there will be one `JSON` formatted file named after the `id` of the test. `id` of the test is generated using the combined hash of `GMX` binary, `libgromacs` and test start time in milliseconds as follows:

`<combined hash of gmx binary and libgromacs>_<hash of test start time in milliseconds>`

Tree view of the `reports` directory:

```
reports
└─── gromacs_version
│   └─── cluster_name
│       └─── partition_name
│           └─── <test_id>.json
│           └─── <test_id>.json
└─── gromacs_version
│   └─── cluster_name
│       └─── partition_name
│           └─── <test_id>.json
│           └─── <test_id>.json
```
A sample `JSON` formatted report file is as follows:
```
{
	"meta": {
		"name": "GromacsRegressionTest on Tegner:interactive_gmx_module_gpu using gromacs_2020_1_avx2",
		"id": "5104496d7e_93e2ace17e",
		"time": "2020-12-01 11:14:56.176672"
	},
	"machine": {
		"cpu": {
			"brand": "Intel(R) Xeon(R) CPU E5-2690 v3 @ 2.60GHz"
		},
		"gpu": {
			"card": [
				"NVIDIA Tesla K80, compute cap.: 3.7, ECC: yes, stat: compatible"
			]
		}
	},
	"binaries": {
		"gmx": {
			"version": "2020.1",
			"precision": "single",
			"simd": "AVX2_256",
			"bin_lib_hash": "5104496d7e"
		}
	},
	"compilers": {
		"C": {
			"version": "GNU 7.2.0",
			"flags": "-mavx2 -mfma -pthread -fexcess-precision=fast -funroll-all-loops"
		},
		"C++": {
			"version": "GNU 7.2.0",
			"flags": "-mavx2 -mfma -pthread -fexcess-precision=fast -funroll-all-loops -fopenmp"
		},
		"CUDA": {
			"version": "release 10.0, V10.0.130",
			"flags": "-std=c++14;-gencode;arch=compute_30,code=sm_30;-gencode;arch=compute_35,code=sm_35;-gencode;arch=compute_37,code=sm_37;-gencode;arch=compute_50,code=sm_50;-gencode;arch=compute_52,code=sm_52;-gencode;arch=compute_60,code=sm_60;-gencode;arch=compute_61,code=sm_61;-gencode;arch=compute_70,code=sm_70;-gencode;arch=compute_35,code=compute_35;-gencode;arch=compute_50,code=compute_50;-gencode;arch=compute_52,code=compute_52;-gencode;arch=compute_60,code=compute_60;-gencode;arch=compute_61,code=compute_61;-gencode;arch=compute_70,code=compute_70;-gencode;arch=compute_75,code=compute_75;-use_fast_math;;-gencode;arch=compute_37,code=sm_37;-mavx2 -mfma -pthread -fexcess-precision=fast -funroll-all-loops -fopenmp",
			"driver": "10.20",
			"runtime": "10.0"
		}
	},
	"libraries": {
		"FFTW": "fftw-3.3.8-sse2-avx-avx2-avx2_128",
		"MPI": "MPI"
	},
	"input": {
		"integrator": "md",
		"dt": "0.002",
		"nsteps": "200000"
	},
	"gmx_log_info": {
		"steps_override": [],
		"nstlist": [
			"Changing nstlist from 10 to 100, rlist from 1.2 to 1.338"
		],
		"bonded_kernel": [
			"Using GPU 8x8 nonbonded short-range kernels"
		],
		"pme_grid": [
			"step 3200: timed with pme grid 40 40 40, coulomb cutoff 1.282: 545.9 M-cycles",
			"step 4000: timed with pme grid 42 42 42, coulomb cutoff 1.221: 507.9 M-cycles",
			"              optimal pme grid 44 44 44, coulomb cutoff 1.200",
			"step 3400: timed with pme grid 42 42 42, coulomb cutoff 1.221: 513.3 M-cycles",
			"step 2600: timed with pme grid 44 44 44, coulomb cutoff 1.200: 500.5 M-cycles",
			"step 4200: timed with pme grid 44 44 44, coulomb cutoff 1.200: 488.5 M-cycles",
			"step 3600: timed with pme grid 44 44 44, coulomb cutoff 1.200: 489.9 M-cycles",
			"step 3800: timed with pme grid 40 40 40, coulomb cutoff 1.282: 545.6 M-cycles",
			"step 2800: timed with pme grid 40 40 40, coulomb cutoff 1.282: 543.6 M-cycles",
			"step 3000: timed with pme grid 36 36 36, coulomb cutoff 1.424: 629.3 M-cycles"
		],
		"update_groups": [
			"nr 6471, average size 2.8 atoms, max. radius 0.139 nm"
		]
	},
	"performance": {
		"ns/day": 90.765,
		"num_tasks": 4
	}
}
```
#### statichtml
This folder contains a combined html file with links to static html file generated for each test. At this moment, static html for each test display the correspondin report file in a formetted way. Tree view of the `statichtml` directory:

```
statichtmls
└─── index.html
└─── tests
│   └─── <test_id>.html
│   └─── <test_id>.html
```


#### `gmx-outputs`
This directory contains all the output generated by `GROMACS`. Structure of this directory is similar to `reports`, except that in this case test `id` is used to create folder within `cluster` directory and contains the `GROMACS` output. Tree view of the `gmx-outputs` directory:

```
gmx-outputs
└─── gromacs_version
│   └─── cluster_name
│       └─── partition_name
│           └─── <test_id>
│               └─── gmx_md.log
│               └─── gmx_md.edr
│               └─── gmx_md.gro
│               └─── gmx_md.cpt
│               └─── gmx_md.xtc
└─── gromacs_version
│   └─── cluster_name
│       └─── partition_name
│           └─── <test_id>
│               └─── gmx_md.log
│               └─── gmx_md.edr
│               └─── gmx_md.gro
│               └─── gmx_md.cpt
│               └─── gmx_md.xtc
```

#### `logs`
This folder contains the following temporary files that will be overriden or deleted during/after each test:

1. `kill-reframe.sh`
2. `reframe.log`
3. `reframe.out`

`gmxbm.py` makes [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) to run on the background. `kill-reframe.sh` allows a user to kill a test prematurely that is runing on background.

`reframe.log` is generated by [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) contains runtime log from [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html). Initially the file resides in the output directory and is moved to `logs` folder within output directory once [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) return from execution.

The content of `reframe.out` is the `stdout` message from [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html).


In addition to the above folders, [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) also generates two temporary folders named `stage` and `output` within the output directory. `stage` folders contains generated job related scripts and output from `GROMACS`. Once the test is done, job related scripts are moved to `output` directory and `GROMACS` outputs are deleted.

---

## `aggregate` module
The job of this module is to aggregate benchmark results from multiple directories into a single user specified directory. All the input directory should contain the following direcrotires and having the previously specified directory structure:

- `reports`
- `gmx-outputs`
- `perflogs`
- `statichtml`

This module accepts a space separated list of input directoris and an output directory. The script basically copy all test related files (*.json, *.html etc) within the above folders from each of the input directory to the approproate folder within the output directory yet maintaining the original directory tree structure. For example, let say a input directory contains a `JSON` file named `my_test.json` within the directory tree `reports/Tegner/queue/my_test.json`. This script will copy the `JSON` file to the output directory maintaining the directory tree `reports/Tegner/queue/my_test.json`. If the output directory already contains the directory tree `reports/Tegner/queue/`, it will just copy the `my_test.json` file to the directory without removing the existing files.

    $ ./gmxbm.py aggregate -h/--help
    usage: ./gmxbm.py aggregate [-h] [-i INPUTS [INPUTS ...]] -o OUTPUT

A sample command may look like this:
```
./gmxbm.py aggregate -i <path to input directory_01> <path to input directory_02> -o <path to output directory>
```
