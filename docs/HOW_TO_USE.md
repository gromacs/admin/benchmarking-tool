## How to use the tool

This section describes how a user can perform a GROMACS benchmark/regression test in a cluster or in a local machine using the tool.

#### Requirement for using the tool manually
* `GROMACS`
* `Python 3.6 of above`
* [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html)


#### Running `run` module of gmxbm.py

    $ ./gmxbm.py run -h/--help
    usage: ./gmxbm.py run [-h] -i INPUT [-C CONFIG_FILE]
                        [--machines [cluster:partition [cluster:partition ...]]]
                        [--prog-envs [PROG_ENVS [PROG_ENVS ...]]]
                        [--nnodes NNODES] [--nprocs NPROCS] [--ntomp NTOMP]
                        [--gpu] [--mpi] [--envs [name:value [name:value ...]]]
                        [--prepend-env] [--outputdir OUTPUTDIR]
                        [--wall-time <days>d<hours>h<minutes>m<seconds>s] --gmx
                        GMX [--descr DESCR]
                        [--maintainers [MAINTAINERS [MAINTAINERS ...]]]



It is possible to run multiple test based on `partition` and `programming environment` choices in a `cluster`. This is enabled by excepting multiple values for `--machines` and `--prog-envs` options. A new test instance will be generated for each combination of `--machines` and `--prog-envs` value.


This tool comes with a builtin machine config file that can be used to run `GROMACS` benchmark test in one's own machine or at `PDC` cluster (interactively or by using the queue system). Location to the builtin config file is `benchmarking-tool/src/configurations/config.py`. This is the default config file that the tool uses in case user does not specify a config file explicitly through command line argument `--config-file`. It is possible to modify this file to add more `cluster:partition` configuration. Below we will go through the commands required to run test using this builtin config file for some of the cases:

###### Running on one's own machine
To run the tool using one's default machine setting, one can use the following command:  
```
./gmxbm.py run --input <path to .tpr file> --machines generic:default --prog-envs builtin --outputdir <path to output dir> --gmx gmx --envs PATH:<path to gmc binary> LD_LIBRARY_PATH:<path to libgromacs>
```
If `GROMACS` is not available through environment module, it is required that user specify the path to the `GROMACS` binary and library through `--envs` option as specified in the above command. Above command uses a programming environment (`builtin`) specified by `--prog-envs`, which is not defined using environment module. The binary and library is used to generate a combined hash to create the test `id`.

###### Running on interactive node at `PDC Tegner`
To run the tool interactively using for `mpi` (`gmx_mpi`) version of `GROMACS` that is available as a `GROMACS` module at `PDC Tegner`, one can use the following command:
```
./gmxbm.py run --input <path to .tpr file> --nprocs 4 --machines Tegner:interactive_gmx_module --prog-envs gromacs_2020_1_avx2 --mpi --ntomp 6 --outputdir <path to output dir> --gmx gmx_mpi
```
The builtin config file defines a cluster named `Tegner` and a partition named `interactive_gmx_module` under that, which uses `gromacs_2020_1_avx2` as one of it's programming environment. The programming environment `gromacs_2020_1_avx2` is defined using a `GROMACS` module available through `PDC Tegner's` module system named `gromacs/2020.1-avx2`. As, in this case, the `GROMACS` binary and library is available through environment module, user does not have to specify `--envs` option to provide the path to the `GROMACS` library and library. The script will use the module system to figure out the path. For the same reason, user does not have to specify `--envs` option in the following command. User can specify his own machine config file by providing the path to the config file using the `-C/--config-file` option. If absolute path is not given, the path will be resolved against the working directory.

###### Running on Queue System at `PDC Beskow`
To run the tool to the Queue System of `PDC Beskow`, use the following command:
```
./gmxbm.py run --input <path to .tpr file> --nprocs 8 --nnodes 2 --machines Beskow:queue --mpi --ntomp 6 --outputdir <path to output dir> --gmx gmx_mpi --wall-time 2h --prog-envs gromacs_2020_4
```
`PDC Beskow` defines a module for `GROMACS` named `gromacs/2020.4` and the builtin config file defined a `cluster:partition` for `PDC Beskow` that uses that module in it's `gromacs_2020_4` programming environment. In the above command we use `gromacs_2020_4` programming environment to run a test for 8 processes and 2 nodes and using the Queue system at `PDC Beskow`. User can specify his own machine config file as well as described in the previous example.
