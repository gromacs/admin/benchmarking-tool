'''
    Author:
        * Muhammed Ahad <maaahad@gmail.com, maaahad@kth.se, ahad3112@yahoo.com>
    Desctiption:
        Jobs of this module are:
            - Generate JSON formatted machie readable report file for each test
            - Generate static HTML for each test report
'''
# Python std
import os
import json
import datetime

# ReFrame

# in-house
from utilities.formatter import create_report_html

class GMXPerformanceReport:
    def __init__(self, test, outputdir):
        reports_dir = os.path.join(
             outputdir,
            (f'reports/{test.gromacs_info["version"]}/'
             f'{test.current_system.name}/{test.current_partition.name}')
        )
        static_html_dir = os.path.join(
             outputdir,
             'statichtml/tests'
        )

        if not os.path.exists(reports_dir):
            os.system(f'mkdir -p {reports_dir}')

        if not os.path.exists(static_html_dir):
            os.system(f'mkdir -p {static_html_dir}')

        self.__static_html_file = os.path.join(static_html_dir, f'{test.id}.html')
        self.__report_file = os.path.join(reports_dir, f'{test.id}.json')
        self.__report = {
            'meta': {
                'name': test.info(),
                'id': test.id,
                'time': str(datetime.datetime.now()),
            },
            'machine': test.machine_info,
            'binaries': test.binaries_info,
            'compilers': test.compilers_info,
            'libraries': test.libraries_info,
            'input': test.sim_input_info,
            # 'output dir': test.storage,
            'gmx_log_info': test.gmx_log_info,
            'performance': test.performance_info
        }


    def report(self):
        # Generate html file
        create_report_html(self.__static_html_file,
                           self.__report,
                           self.__report.get('meta', {}).get('id', 'Not available')
        )
        # generate json file
        with open(self.__report_file, 'w') as f:
            json.dump(self.__report, f, indent='\t')
